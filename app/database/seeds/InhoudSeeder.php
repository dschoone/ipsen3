<?php
/**
 * Created by PhpStorm.
 * User: Daan
 * Date: 22-1-14
 * Time: 16:42
 */

//Om alleen deze seeder te runnen gebruik je 'php artisan db:seed --class=InhoudSeeder'
class InhoudSeeder extends Seeder
{
    public function run()
    {
        DB::table('inhoud')->delete();
        DB::table('reviews')->delete();
        $review = new Review;
        $review->title = 'Beste service';
        $review->userid = 1;
        $review->voorpagina = 1;
        $review->text = 'beste bedrijf in de geschiedenis van ooit';
        $review->save();
        
        $inhoud = new Inhoud;
        $inhoud->beschrijving = 'LeenmeijIdentiteit';
        $inhoud->textnl = 'Leenmeij combineert de kracht van een sterk merk met een gedegen kennis van de lokale markt, flexibiliteit en een goede service zodat u waar voor uw geld krijgt. Opgericht in 1999, Leenmeij Autoverhuur is gelicentieerd in Nederland.';
        $inhoud->texten = 'Leenmeij combines the strength of one of the country\'s largest car rental brands together with the detailed local knowledge, flexibility and great value for money you need. Established in 1999, the Leenmeij Rent a Car brand is licenced within the Netherlands.';
        $inhoud->save();

        $inhoud = new Inhoud;
        $inhoud->beschrijving = 'LeenmeijVisie';
        $inhoud->textnl = 'Bij Leenmeij staat een goede prijs/kwaliteits verhouding centraal. We bieden u een aantal vroegboekkortingen aan evenals extra voorzieningen die u in staat stellen te besparen op mogelijke extra kosten.';
        $inhoud->texten = 'We\'ve always been offering car rentals with great value for money. We now offer a range of pre-payment options and charge facilities designed to keep you in control and help you save money.';
        $inhoud->save();

        $inhoud = new Inhoud;
        $inhoud->beschrijving = 'LeenmeijService';
        $inhoud->textnl = 'Al onze voertuigen zijn zorgvuldig schoongemaakt en gecontroleerd voor elke verhuur. Voortdurend wordt ons wagenpark vernieuwd om u de meest moderne modellen tegen aantrekkelijke prijzen te kunnen aanbieden.';
        $inhoud->texten = 'For your peace of mind all our vehicles are safety checked and cleaned before every rental. And because we are constantly renewing our fleet you’ll be driving some of the very latest model vehicles all at low prices.';
        $inhoud->save();

        $inhoud = new Inhoud;
        $inhoud->beschrijving = 'LeenmeijMotto';
        $inhoud->textnl = 'U zoekt: wij leveren.';
        $inhoud->texten = 'Whatever your rental needs - we can deliver.';
        $inhoud->save();

       

        $inhoud = new Inhoud;
        $inhoud->beschrijving = 'LeenmeijGeschiedenis';
        $inhoud->textnl = 'Een korte historie van Leenmeij.<br>leenmeij is opgericht in 1999 in Leiden met een geheel eigen manier van auoverhuur, de \"Leenmeij-verhuurmethode\" voor particulieren.<br>De meeste concurrentie was geconcentreerd rond het vliegveld Schiphol, maar Leenmeij veroverde de markt door op prijsgebied de competitie aan te gaan. Met 3 grote concurrenten op het vliegveld die gemiddeld 50 Euro per dag rekenden en een km-prijs van 30 cent, startten Leen en zijn vrouw met een tiental voertuigen en een prijs van 40 Euro en 20 cent per km.<br>2 jaar later was Leenmeij Autoverhuur een begrip geworden in de Leidse regio en de basis werd gelegd voor het uitbreiden met filialen en franchise-nemers. Sindsdien blijft Leenmeij Autoverhuur trouw aan het principe van het leveren van goede diensten tegen aantrekkelijke prijzen.';
        $inhoud->texten = 'A brief history of Leenmeij.<br> Leenmeij was founded by Leen van der Meij in 1999 in Leiden, the Netherlands as a rental car company for the \"Leenmeij-minded\" personal renter.<br>It broke into an already crowded market by operating off-airport and offering more competitive rates than its on-airport competitors. With three established on-airport competitors charging Euro 50 a day and 20 cents a kilometer, Leen and his wife singlehandedly managed a fleet of 10 cars available for Euro 40 a day and 20cents a kilometer.<br>Two years later, Leenmeij Rent-a-Car was established, and the foundation of a system of corporate-owned and licensee-owned operations was laid. Since that time, Leenmeij Renta-Car has continued to stay true to its name, providing renters with the same value for money, quality service.';
        $inhoud->save();

        $inhoud = new Inhoud;
        $inhoud->beschrijving = 'LeenmeijNu';
        $inhoud->textnl = 'Leenmeij Autoverhuur is nu uitgegroeid tot Leiden\'s 3e Autoverhuurbedrijf, met gemiddeld 20 werknemers en een verhuurbestand van rond de 60 voertuigen.';
        $inhoud->texten = 'Today, Leenmeij Rent-a-Car has grown to become Leiden\'s third largest car and van rental company. Leenmeij employs approximately 20 people and operates an average fleet of over 60 cars and vans from approximately their Leiden location.';
        $inhoud->save();
    }
}