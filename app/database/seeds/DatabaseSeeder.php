<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

		Eloquent::unguard();

		// $this->call('UserTableSeeder');

        $this->call('SentrySeeder');
        $this->command->info('Sentry tables seeded!');

        $this->call('InhoudSeeder');
        $this->command->info('Inhoud is in de database toegevoegd');

		//Eloquent::unguard();


	}

}