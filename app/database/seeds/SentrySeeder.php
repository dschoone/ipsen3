<?php
/**
 * Zorgt dat de minimaal benodigde gegevens worden ingevoerd in de database
 * @author Daan
 * Date: 21-1-14
 * Time: 16:31
 */

class SentrySeeder extends Seeder{

    public function run()
    {
        DB::table('users')->delete();
        DB::table('groups')->delete();
        DB::table('users_groups')->delete();

        $adminGebruiker = Sentry::getUserProvider()->create(array(
            'email'          => 'admin@admin.com',
            'gebruikersnaam' => 'admin',
            'password'       => "test",
            'voornaam'       => 'Peter',
            'activated'      => 1,
        ));

        $adminGroep = Sentry::getGroupProvider()->create(array(
            'name'        => 'admin',
            'permissions' => array(
                'admin'         => 1,
                'gebruikers'    => 1,
                'particulier'   => 1,
                'zakelijk'      => 1,
            ),
        ));

        $gebruikerGroep = Sentry::getGroupProvider()->create(array(
            'name'        => 'gebruikers',
            'permissions' => array(
                'admin'         => 0,
                'gebruikers'    => 0,
                'particulier'   => 0,
                'zakelijk'      => 0,
            ),
        ));

        $particulierGroep = Sentry::getGroupProvider()->create(array(
            'name'        => 'particulier',
            'permissions' => array(),
        ));

        $zakelijkGroep = Sentry::getGroupProvider()->create(array(
            'name'        => 'zakelijk',
            'permissions' => array(),
        ));

        // Assign user permissions
        $adminGebruiker->addGroup($adminGroep);
    }

} 