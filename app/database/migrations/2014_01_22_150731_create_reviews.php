<?php

use Illuminate\Database\Migrations\Migration;

class CreateReviews extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('reviews', function($table)
        {
            $table->increments('id');
            $table->integer('userid');
            $table->foreign('userid')->references('id')->on('users');
            $table->integer('reservering')->nullable();
            $table->text('text');
            $table->string('title');
            $table->boolean('voorpagina')->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('reviews');
	}

}
