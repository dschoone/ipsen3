<?php

use Illuminate\Database\Migrations\Migration;

class CreateInhoud extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('inhoud', function($table)
        {
            $table->increments('id');
            $table->string('beschrijving');
            $table->text('textnl');
            $table->text('texten');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('inhoud');
	}

}