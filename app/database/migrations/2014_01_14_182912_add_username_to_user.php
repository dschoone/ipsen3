<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsernameToUser extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->string('gebruikersnaam')->unique();
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->string('voornaam')->nullable();
            $table->string('tussenvoegsel')->nullable();
            $table->string('achternaam')->nullable();
            $table->string('bsn')->nullable();
            $table->string('kvk')->nullable();
            $table->string('bedrijfsnaam')->nullable();
            $table->string('straatnaam')->nullable();
            $table->string('huisnummer')->nullable();
            $table->string('postcode')->nullable();
            $table->string('plaats')->nullable();
            $table->string('provincie')->nullable();
            $table->string('land')->nullable();
            $table->string('telefoonnummer')->nullable();
            $table->string('mobielnummer')->nullable();
            $table->boolean('zakelijk')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}