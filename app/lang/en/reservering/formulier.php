<?php

/* 
 * Author: Bart
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return array(
    'begindatum'        => 'Date of Rent',
    'einddatum'         => 'End Date',
    'kilometers'        => 'Expected Kilometers',
    'voertuig'          => 'What kind of vehicle do you want?',
    'scooter'           => 'Scooter',
    'auto'              => 'Passenger Car',
    'bus'               => 'Van',
    'zakelijkkeuze'     => 'Business or Personal?',
    'zakelijk'          => 'Business',
    'particulier'       => 'Personal',
    'beschikbaarlijst'  => 'Availability'
    
    
    
);

