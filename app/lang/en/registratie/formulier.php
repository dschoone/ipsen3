<?php
/**
 * Created by PhpStorm.
 * User: Daan
 * Date: 22-1-14
 * Time: 14:03
 */

return array(
    'gebruikersnaam'            => 'Username*',
    'wachtwoord'                => 'Password* :extra',
    'voornaam'                  => 'Firstname*',
    'achternaam'                => 'Lastname*',
    'tussenvoegsel'             => 'Surname',
    'email'                     => 'E-mail*',
    'bsn'                       => 'Citizen Service Number',
    'kvk'                       => 'Chamber of Commerce',
    'bedrijfsnaam'              => 'Name of Organisation',
    'straatnaam'                => 'Street-name*',
    'huisnummer'                => 'House Number*',
    'postcode'                  => 'Zip Code*',
    'telefoonnummer'            => 'Phone Number*',
    'mobielnummer'              => 'Mobile Number',
    'zakelijk'                  => 'Business',
    'verstuur'                  => 'Send',
    'persoonlijkeinformatie'    => 'Personal Information',
    'tweet'                     => 'Newest Tweets',
    'herhaal'                   => 'Repeat',
    'registratie'               => 'Register',
    'anderecaptcha'             => 'Other Picture',
	'captcha'                   => 'Captcha*',
);