<?php
/**
 * Created by PhpStorm.
 * User: Daan
 * Date: 22-1-14
 * Time: 14:06
 */
return array(
    'bestaandegebruikersnaam'               => 'De gebruikersnaam die u heeft opgegeven bestaat helaas al. Probeert u het alstublieft opnieuw met een andere gebruikersnaam',
    'bestaandeemail'                        => 'Het e-mailadres wat u heeft opgegeven bestaat helaas al. Probeert u het alstublieft opnieuw met een ander e-mailadres',
    'verschillendwachtwoord'                => 'De wachtwoorden die u heeft ingevuld komen niet met elkaar overeen. Probeert u het alstublieft opnieuw.',
    'geenwachtwoord'                        => 'U heeft geen wachtwoord ingevuld. Probeert u het alstublieft opnieuw met een wachtwoord.',
    'ietsmisgegaan'                         => 'Er is helaas iets mis gegaan tijdens het registreren. Probeert u het alstublieft opnieuw.',
    'geenkvk'                               => 'U heeft aangegeven dat u een zakelijke klant bent, maar u heeft geen KVK nummer opgegeven. Probeert u het alstublieft opnieuw met een juist KVK nummer.',
);