<?php
/**
 * Created by Bart
 * Date: 22-1-14
 * Time: 14:44
 * Engels
 */
return array(
    'login'     => 'Log In',
    'loguit'    => 'Log :naam Out',
    'taal'      => 'Language', // Plaatje van vlag
    'reserveer' => 'Order Right Away',
    'verzend'   => 'Send'
);