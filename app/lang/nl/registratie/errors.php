<?php
/**
 * Created by PhpStorm.
 * User: Daan
 * Date: 22-1-14
 * Time: 14:06
 */
return array(
	'verschillendwachtwoord'  => 'De wachtwoorden die u heeft ingevuld komen niet met elkaar overeen. Probeert u het alstublieft opnieuw.',
	'geenwachtwoord'          => 'U heeft geen wachtwoord ingevuld. Probeert u het alstublieft opnieuw met een wachtwoord.',
	'ietsmisgegaan'           => 'Er is helaas iets mis gegaan tijdens het registreren. Probeert u het alstublieft opnieuw. Zie hier benenden voor meer informatie.',
	'captcha'                 => 'U heeft de captcha verkeerd ingevuld. Probeert u het alstublieft opnieuw.',
	'gebruikersnaambenodigd'  => 'U heeft heeft geen gebruikersnaam ingevuld. Probeert u het alstublieft opnieuw',
	'passwordbenodigd'        => 'U heeft heeft geen password ingevuld. Probeert u het alstublieft opnieuw',
	'kvkbenodigd'             => 'U heeft aangegeven dat u een zakelijke klant bent, maar u heeft geen KVK nummer opgegeven. Probeert u het alstublieft opnieuw met een juist KVK nummer.',
	'voornaambenodigd'        => 'U heeft heeft geen voornaam ingevuld. Probeert u het alstublieft opnieuw',
	'achternaambenodigd'      => 'U heeft heeft geen achternaam ingevuld. Probeert u het alstublieft opnieuw',
	'kvkbenodigd'             => 'U heeft heeft geen kvk ingevuld. Probeert u het alstublieft opnieuw',
	'bedrijfsnaambenodigd'    => 'U heeft heeft geen bedrijfsnaam ingevuld. Probeert u het alstublieft opnieuw',
	'emailbenodigd'           => 'U heeft heeft geen email ingevuld. Probeert u het alstublieft opnieuw',
	'telefoonnummerbenodigd'  => 'U heeft heeft geen telefoonnummer ingevuld. Probeert u het alstublieft opnieuw',
);