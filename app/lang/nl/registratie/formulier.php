<?php
/**
 * Created by PhpStorm.
 * User: Daan
 * Date: 22-1-14
 * Time: 14:03
 */

return array(
    'gebruikersnaam'            => 'Gebruikersnaam*',
    'wachtwoord'                => 'Wachtwoord* :extra',
    'voornaam'                  => 'Voornaam*',
    'achternaam'                => 'Achternaam*',
    'tussenvoegsel'             => 'Tussenvoegsel',
    'email'                     => 'E-mail*',
    'bsn'                       => 'BSN',
    'kvk'                       => 'KvK',
    'bedrijfsnaam'              => 'Bedrijfsnaam',
    'straatnaam'                => 'Straatnaam*',
    'huisnummer'                => 'Huisnummer*',
    'postcode'                  => 'Postcode*',
    'telefoonnummer'            => 'Telefoon nummer*',
    'mobielnummer'              => 'Mobiele nummer',
    'zakelijk'                  => 'Zakelijk',
    'verstuur'                  => 'Verstuur',
    'persoonlijkeinformatie'    => 'Persoonlijke informatie',
    'tweet'                     => 'Nieuwste Tweets',
    'herhaal'                   => 'Herhalen',
    'captcha'                   => 'Captcha*',
    'anderecaptcha'             => 'Andere afbeelding',
    'registratie'               => 'Registratie',
);