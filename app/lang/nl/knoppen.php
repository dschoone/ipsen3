<?php
/**
 * Created by Bart
 * Date: 22-1-14
 * Time: 14:44
 * Nederlands
 */
return array(
    'login'     => 'Log In',
    'loguit'    => 'Log :naam Uit',
    'taal'      => 'Taal', // Plaatje van vlag
    'reserveer' => 'Direct Reserveren',
    'verzend'   => 'Verzenden'
);