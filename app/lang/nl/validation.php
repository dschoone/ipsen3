<?php
/**
 * Created by PhpStorm.
 * User: Daan
 * Date: 5-2-14
 * Time: 11:42
 */ 

return array(

	"alpha"            => ":attribute moet uit alleen letters bestaan.",
	"alpha_dash"       => ":attribute mag alleen letters, cijfers en leestekens bevatten.",
	"alpha_num"        => ":attribute mag alleen letters en cijfers bevatten.",
    "captcha"     => ":attribute komt niet overeen met de afbeelding.",
	"email"            => ":attribute formaat is invalide.",
	"max"              => array(
		"numeric" => ":attribute mag niet groter zijn dan :max.",
		"file"    => ":attribute mag niet groter zijn dan :max kilobytes.",
		"string"  => ":attribute mag niet groter zijn dan :max karakters.",
		"array"   => ":attribute mag niet meer zijn dan :max items.",
	),
	"min"              => array(
		"numeric" => ":attribute moet renminste :min bevatten.",
		"file"    => ":attribute moet tenminste :min kilobytes bevatten.",
		"string"  => ":attribute moet tenminste :min karakters bevatten.",
		"array"   => ":attribute moet tenminste :min items bevatten.",
	),
	"numeric"          => ":attribute moet een nummer zijn.",
    "unique"           => ":attribute is al in gebruik.",
	"required"         => ":attribute veld is verplicht.",
	"required_if"      => ":attribute veld is verplicht als :other :value bevat.",
	"same"             => ":attribute en :other komen niet overeen.",
    "password"	=> ":attribute voldoet niet aan de eisen: grote en kleine letter en een cijfer en minimum lengte van 8 tekens",
    "postcode"         => ":attribute heeft de verkeerde format.",
/*
	'custom' => array(
		"password"	=> "Wachtwoord voldoet niet aan de eisen: grote en kleine letter en een cijfer en minimum lengte van 8 tekens", 
	),
*/

	'attributes' => array(
        "postcode" => "Postcode",
        "gebruikersnaam" => "Gebruikersnaam",
		"email"	=> "E-mail adres",
        "achternaam" => "Achternaam",
        "password" => "Wachtwoord",
        "password2" => "Wachtwoord Herhaald",
	),




);