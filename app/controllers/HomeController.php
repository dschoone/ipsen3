<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/


	
    
    
        public function showWelcome()
	{	
            $reviews =array();
            $inhoud =Inhoud::where('beschrijving','=','LeenmeijIdentiteit')->firstOrFail();
            $reviews = Review::all();
            $reviewen =array();
            foreach($reviews as $review)
            {
                if ($review->voorpagina == true)
                {
                $reviewen[$review->title] = array('titel'=>$review->title,'tekst'=>$review->text);
                }
            }
            
           return View::make('WelcomePage')->with (array('inhoud'=> $inhoud,'reviews'=>$reviewen )); 
                

	}
        public function showReviews()
        {
            $reviews =array();
            $reviewsdb =Review::all();
            foreach ($reviewsdb as $review)
            {
                
                $user = User::find($review->userid);
                $usernaam = $user->voornaam;
                $reviews[$review->id] = array('title'=>$review->title,'text'=>$review->text,'naam'=>$usernaam);
            }
            return View::make('ReviewPage')-> with('reviews',$reviews);
        }
        public function showInfo()
        {
            $inhouds =array();
            $inhouden = Inhoud::all();
            foreach($inhouden as $inhoud)
            {
                $inhouds[$inhoud->beschrijving]= array('beschrijving'=>$inhoud->beschrijving,'textnl'=>$inhoud->textnl,'texten'=>$inhoud->texten);
            }
            
            return View::make('InfoPage')-> with('inhoud',$inhouds);
        }
        public function showReviewMaak()
        {
         return View::make('ReviewView') ;    
        }
        

}