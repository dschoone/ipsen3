<?php
/**
 * @author:Bonnie
 * @Purpose: This is use for all movements concerning the LogIN.
 * 
 */
class LogInController extends BaseController
{
    // Directs the user to the Registration page
    public function ShowRegisterFormulier()
    {
        return View::make('registratiepagina');
    }
    
    // Directs the user to the Login page
    public function ShowLoginpagina()
    {
        return View::make('inlogpagina');
    }
    
    // Directs the user to the Login page
    public function ShowWachtwoordReset()
    {
        return View::make('ResetPagina');
    }
}

