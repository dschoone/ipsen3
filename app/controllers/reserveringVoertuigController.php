
<?php
    /*Controller voor het uitvoeren van alle zaken die te maken hebben met het reserveren
     * Auteur : Martijn
     */

class reserveringVoertuigController extends BaseController
{   


    public $restful =true;
    private $startDatum;
    private $eindDatum;
    private $voertuigType;
    private $kilometers;
   

  
    /*
     * Toont reserverings pagina met alle voertuigen
     * Auteur : Martijn
     */
     public function show_reserveringspagina()
             
     {
         $voertuigen = Voertuig::all();
         $beschikbareVoertuigen =array();
         foreach($voertuigen as $voertuig)
         {
             $categorie = Lang::get('voertuig.'.$voertuig->categorie);
              $beschikbareVoertuigen[$voertuig->id]= array('id'=> $voertuig->id,'categorie'=> $categorie,'merk'=>$voertuig->merk,'type'=>$voertuig->type,'kleur'=>$voertuig->kleur,'beschrijving'=>$voertuig->beschrijving,'bedrag'=>'€'.$this->bepaalBedrag($voertuig->id, 'particulier', '2014-01-01', '2014-01-02', '10'),'reserveren'=>false);
         }
         return View::make('ReserveringsPagina')->with('beschikbareVoertuigen',$beschikbareVoertuigen);
          //return View::make('TestR')->with('beschikbareVoertuigen',$beschikbareVoertuigen);
     }
     /* valideert gegevens die zijn opgegeven voor reserverings filter
      * Auteur : Martijn
      */
    public function showBeschikbareVoertuigen()
    {
        Session::put('zakelijk',$_POST['zakelijk']);
        if (Sentry::check()){
        $user = Sentry::getUser();

        if( $user->zakelijk == true)
        {
           Session::put('zakelijk','zakelijk');

        };
        }
        Session::put('startDatum',$_POST['beginDatum']);
        Session::put('eindDatum',$_POST['eindDatum']);
        Session::put('voertuigType',$_POST['voertuigType']);
        Session::put('kilometers',$_POST['kilometers']);
        //Session::put('zakelijk',$_POST['zakelijk']);
        $this->startDatum = Session::get('startDatum');
        $this->eindDatum =Session::get('eindDatum');
        $this->voertuigType =Session::get('voertuigType');
        $this->kilometers = Session::get('kilometers');
        $valid = true;
        if($this->startDatum < date("Y-m-d"))
        {
            
            $valid = false;
            return Redirect::to('reserveer')->with('message', 'Startdatum is al geweest');
        }
        if($this->startDatum > $this->eindDatum)
        {
            
            $valid =false;
            return Redirect::to('reserveer')->with('message', 'startDatum is na eind datum');

        }
        
        if ($this->kilometers <=0)
        {

            
            $valid = false;
            return Redirect::to('reserveer')->with('message', 'vul het aantal kilometers in');
        }
        
        if ($valid)
        {
            return $this->getBeschikbareVoertuigen($this->startDatum,$this->eindDatum,$this->voertuigType);

        }
        
        
      
    }
    /* geeft een view terug met de beschikbare voertuigen aan de hand van klant informatie
     * over start en einddatum en het voertuig type dat de klant wil reserveren
     * Auteur : Martijn
     */
    public function getBeschikbareVoertuigen($startDatum,$eindDatum,$voertuigType)
    {
        $reserveringen =  Reservering::all();
        $voertuigen = Voertuig::all();
        $beschikbareVoertuigen = array();

        foreach($voertuigen as $voertuig)
        {
            $check = true;
            if($voertuig->type == $voertuigType)
            {
                foreach ($reserveringen as $reservering)
                {

                    if($voertuig->id == $reservering->voertuigenid)
                    {

                        if ($startDatum <= $reservering->einddatum  and  $reservering->begindatum <= $eindDatum)
                        {
                            $check = false;
                        }
                    }
                }
                    if ($check == true)
                    {
//                        echo "<pre>";
//                        die(var_dump(Session::get('zakelijk')));
                        if (Session::get('zakelijk')=='particulier')
                        {
                            $bedrag = '€'.$this->bepaalBedrag($voertuig->id, Session::get('zakelijk'), Session::get('startDatum'), Session::get('eindDatum'), Session::get('kilometers')).' Inclusief BTW';
                        }
                        if (Session::get('zakelijk')=='zakelijk')
                        {
                            $bedrag = '€'.$this->bepaalBedrag($voertuig->id, Session::get('zakelijk'), Session::get('startDatum'), Session::get('eindDatum'), Session::get('kilometers')).' Exclusief BTW';
                        }
              
                        $beschikbareVoertuigen[$voertuig->id]= array('id'=> $voertuig->id,'categorie'=> $voertuig->categorie,'merk'=>$voertuig->merk,'type'=>$voertuig->type,'kleur'=>$voertuig->kleur,'beschrijving'=>$voertuig->beschrijving,'bedrag'=>$bedrag,'reserveren'=>true);
                    }         

                }
       }
       return View::make('ReserveringsPagina')->with('beschikbareVoertuigen',$beschikbareVoertuigen);


   
    } 
    
    /* Maakt een reservering aan vooor een klant en stuurt een email bericht.
     * Auteur: Martijn
     */
       public function voertuigReserveren()
       {
           $voertuigInput = Input::get('voertuig');
           if(isset($voertuigInput))
           {
             $voertuigID = Input::get('voertuig');
           }
           else
           {
               $voertuigID= Session::get('voertuigid');
           }
           $startDatum = Session::get('startDatum');
           $eindDatum = Session::get('eindDatum');
           $zakelijk = Session::get('zakelijk');
           $kilometers = Session::get('kilometers');
           $user = Sentry::getUser();
           $userzakelijk ="particulier";
            if ( $user->zakelijk == false)
            {
                $userzakelijk="particulier";
            }
            if( $user->zakelijk == true)
                    {
                $userzakelijk= "zakelijk";
                            
                    };
           
             if($this->checkBeschikbaar($voertuigID,$startDatum,$eindDatum)==true)
                {
                    DB::insert(DB::raw('insert into reservering (klantenid,voertuigenid,reserveerdatum,begindatum,einddatum,kilometer,bedrag,status)
                              Values (:klantenId,:voertuigenId,:reserveerDatum,:beginDatum,:eindDatum,:kilometer,:bedrag,:status)'),
                               array('klantenId'=>$user->id  ,'voertuigenId'=> $voertuigID ,'reserveerDatum'=> date('Y-m-d'),'beginDatum'=>$startDatum,'eindDatum'=>$eindDatum,'kilometer'=>$kilometers,'bedrag'=>$this->bepaalBedrag($voertuigID,$userzakelijk, $startDatum, $eindDatum,$kilometers),'status'=>'gereserveerd'));
                    $data = array('reserveerDatum'=> date('Y-m-d'),'beginDatum'=>$startDatum,'eindDatum'=>$eindDatum,'kilometer'=>$kilometers,'bedrag'=>$this->bepaalBedrag($voertuigID,$zakelijk, $startDatum, $eindDatum,$kilometers),'voertuig'=>Voertuig::find($voertuigID)->beschrijving);
                    Mail::send('emails.reservering',$data,function($message) use($user)
                    {
                        //$message->to('m.wuis@live.nl', 'Martijn Wuis')->subject('Uw Reservering is gemaakt!');
                       $message->to($user->email, $user->voornaam." ".$user->tussenvoegsel." ".$user->achternaam)->subject('Uw Reservering is gemaakt!');
                    });
                    return Redirect::to('/')->with('message','Reservering gemaakt');
                    
                }
           
           
           
       }
       /*Checkt of een voertuig niet al gereserveerd is aan de hand van start en eind datum
        * Auteur: Martijn         
        */
       public function checkBeschikbaar($voertuigId,$startDatum,$eindDatum)
       {
           
           $reserveringen = Voertuig::find($voertuigId)->reservering;
           
           foreach ($reserveringen as $reservering)
           {
               if ($startDatum <= $reservering->einddatum  and  $reservering->begindatum <= $eindDatum)
                {
                    return false;
                }
           }
           return true;

        }
        /* Bepaalt de kosten voor een speciefiek voertuig aan de hand van het aantal dagen en aantal kilometers
         * Auteur: Martijn
         */
        public function bepaalBedrag($voertuigID,$zakelijk,$startDatum,$eindDatum,$kilometers)
        {
           $voertuig = Voertuig::find($voertuigID);   
           $startDateTime = new DateTime($startDatum);
           $eindDateTime = new DateTime($eindDatum);
           
           $dagen = $startDateTime->diff($eindDateTime);
           
           $prijs = ($dagen->days * $voertuig->dagprijs) + ($kilometers * $voertuig->kilometerprijs); 
           if ($zakelijk == 'particulier')
           {
               $prijs = ($prijs *1.21);
               setlocale(LC_MONETARY, 'it_IT');
               $prijs= money_format('%.2n', $prijs);        
               return $prijs;
                       
           }
           if ($zakelijk == 'zakelijk')
           {
               setlocale(LC_MONETARY, 'it_IT');
               $prijs= money_format('%.2n', $prijs); 
               return $prijs;
           }
           
        }
}
 

?>
