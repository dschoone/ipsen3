<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TaalController
 *
 * @author Bart
 */
class TaalController extends BaseController
{
    public function set()
    {
        $locales = array('nl', 'en');
        $locale = Input::get('locale');
        
        if (in_array($locale,$locales))
        {
            Session::put('locale', $locale);
        }
        
        return Redirect::back();
    }
    public function switchEngels()
    {
        Session::put('my.locale', 'en');
        return Redirect::to('/');
    }
    public function switchNederlands()
    {
        Session::put('my.locale','nl');
        return Redirect::to('/');
    }
}
