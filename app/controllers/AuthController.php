<?php


include(app_path().'/securimage/securimage.php');

/**
 * Class AuthController
 * @author Daan
 */
class AuthController extends BaseController
{

    private $errors;

    /**
     * Laat de loginpagina zien
     * @return De login View
     */
    public function getLogin()
    {
        return View::make('inlogpagina');
    }

    /**
     * Verwerkt de gegevens die zijn ingevult in de inlog View. En logt de gebruiker in als de goede gegevens zijn ingevult.
     * @return De (admin) home pagina of de login View met errors
     */
    public function postLogin()
    {
        $gegevens = array(
            'gebruikersnaam' => Input::get('gebruikersnaam'),
            'password' => Input::get('wachtwoord')
        );

        try
        {
            $gebruiker = Sentry::authenticate($gegevens, false);

            if ($gebruiker)
            {
                Sentry::login($gebruiker, false);
                $admin = Sentry::findGroupByName('admin');

                if ($gebruiker->inGroup($admin))
                {
                    return View::make('Admin.HomePage');
                }
                else
                {
                    return Redirect::intended('/');
                }

                
            }
        }
        catch(\Exception $e)
        {
            return Redirect::route('login')->withErrors(array('login' => $e->getMessage()));
        }

    }

    /**
     * Logt de gebruiker uit en stuurt hem weer naar de login View
     * @return De login View
     */
    public function getLogout()
    {
        Sentry::logout();

        return Redirect::route('login');
    }

    /**
     * Laat de registratie View zien waar de bezoeker zich kan registreren.
     * @return De registratie View
     */
    public function getRegistreer()
    {
        return View::make('registratiepagina');
    }

    /**
     * Verwerkt de registratie van een nieuwe gebruiker. De activeringscode wordt verstuurd naar de gebruiker.
     * @return \Illuminate\Http\RedirectResponse
     */

    private function startsWith($haystack, $needle)
    {
        return !strncmp($haystack, $needle, strlen($needle));
    }

    private function makeErrorMessage($messages, $regels) //Hele functie moet opnieuw, ik ben er achter gekomen dat het automatisch de lang files gebruikt voor een error message. Die was er niet voor nederlands, dus nu heb ik gefilterd op de error messages van de lang files ipv een nederlandse validatie lang file te maken. -_-'
    {
        $errors = array();
        foreach(array_keys($regels) as $veld)
        {

            $errors = array_merge($errors, array($veld => $messages->first($veld) ?: ''));

//            $message = $messages->first($veld) ?: 'geenerror';
//            if (! (strcmp($message, 'validation.required') && strcmp($message, 'validation.required_if')))
//            {
//                $bericht = array($veld => Lang::get('registratie/errors.'. $veld . 'benodigd'));
//                $errors = array_merge($errors, $bericht);
//            }
//            else if(strpos($message, 'validation.min') !== false)
//            {
//                $bericht = array($veld => Lang::get('registratie/errors.'. $veld . 'tekort'));
//                $errors = array_merge($errors, $bericht);
//            }
//            else if(strpos($message, 'validation.max') !== false)
//            {
//                $bericht = array($veld => Lang::get('registratie/errors.'. $veld . 'telang'));
//                $errors = array_merge($errors, $bericht);
//            }
//            else if(! strcmp($message, 'validation.alpha_dash'))
//            {
//                $bericht = array($veld => Lang::get('registratie/errors.'. $veld . 'alleenlettersentekens'));
//                $errors = array_merge($errors, $bericht);
//            }
//            else if(! strcmp($message, 'validation.email'))
//            {
//                $bericht = array($veld => Lang::get('registratie/errors.'. $veld . 'verkeerd'));
//                $errors = array_merge($errors, $bericht);
//            }
//            else if(! strcmp($message, 'validation.password'))
//            {
//                $bericht = array($veld => Lang::get('registratie/errors.'. $veld . 'verkeerd'));
//                $errors = array_merge($errors, $bericht);
//            }
        }
        $this->errors = $errors;
//        echo "<pre>";
//        die(var_dump($this->errors));
    }

    public function postRegistreer()
    {
        //$securimage = new Securimage();
/*
        $wachtwoord1 = Input::get('wachtwoord');
        $wachtwoord2 = Input::get('wachtwoord2');
*/
        $regels = array(
            'gebruikersnaam' => 'required|alpha_dash|unique:users',
            'password' => 'required|alpha_dash|password',
            'password2' => 'required|same:password',
            'voornaam' => 'required|name',
            'tussenvoegsel' => 'name',
            'achternaam' => 'required|name',
            'kvk' => 'required_if:zakelijk,true|alpha_num',
            'bedrijfsnaam' => 'required_if:zakelijk,true|name',
            'straatnaam' => 'required|name',
            'huisnummer' => 'required|alpha_num',
            'postcode' => 'required|min:6|max:7|postcode',
            'plaats' => 'name',
            'provincie' => 'name',
            'land' => 'name',
            'email' => 'required|email|unique:users',
            'telefoonnummer' => 'required|min:10|max:12|phone',
            'mobielnummer' => 'min:10|max:12||phone',
            'zakelijk'  => '',
            'captcha_code' => 'required|captcha|alpha_dash',
        );

        try
        {
//            if ($wachtwoord1 == $wachtwoord2)
//            {
                $gegevens = array(
                    'gebruikersnaam' => Input::get('gebruikersnaam'),
                    'password' => Input::get('wachtwoord'),
                    'password2' => Input::get('wachtwoord2'),
                    'voornaam' => Input::get('voornaam'),
                    'tussenvoegsel' => Input::get('tussenvoegsel'),
                    'achternaam' => Input::get('achternaam'),
                    'kvk' => Input::get('kvk'),
                    'bedrijfsnaam' => Input::get('bedrijfsnaam'),
                    'straatnaam' => Input::get('straatnaam'),
                    'huisnummer' => Input::get('huisnummer'),
                    'postcode' => Input::get('postcode'),
                    'plaats' => Input::get('plaats'),
                    'provincie' => Input::get('provincie'),
                    'land' => Input::get('land'),
                    'email' => Input::get('email'),
                    'telefoonnummer' => Input::get('telefoonnummer'),
                    'mobielnummer' => Input::get('mobielnummer'),
                    'zakelijk' => Input::get('zakelijk'),
                    'captcha_code' => Input::get('captcha_code'),
                    'activated' => false,
                );

                $validator = Validator::make($gegevens, $regels);// Nu niet te gebruiken, maar moet er wel in komen als we de eerste kans niet halen

                if ($validator->fails())
                {
                    //die("in de fails");
                    //$messages = $validator->messages();
//                    echo "<pre>";
//                    die(var_dump($messages->getMessages()));
                    //$this->makeErrorMessage($messages, $regels);
                    //$this->makeErrorMessage($messages, $regels);
                    throw new Exception(Lang::get('registratie/errors.ietsmisgegaan'));
                }
                //die("buiten de fails");
/*                if ($gegevens['zakelijk'] == 'true' && $gegevens['kvk'] == null)
                {
                    throw new Exception(Lang::get('registratie/errors.geenkvk'));
                }

*/
//                if ($securimage->check(Input::get('captcha_code')) == false) {
//                    // the code was incorrect
//                    // you should handle the error so that the form processor doesn't continue
//
//                    // or you can use the following code if there is no validation or you do not know how
//
//                    throw new Exception(Lang::get('registratie/errors.captcha'));
//                }
                
                unset($gegevens['password2']);
                unset($gegevens['captcha_code']);
                $gebruiker = Sentry::getUserProvider()->create($gegevens);
                
               // DB::insert(DB::raw('insert into klant (id,klantnummer,voornaam,emailadres)
                 //             Values (:Id,:klantnummer,:voornaam,:email'),array('Id'=>$gebruiker->id,'klantnummer'=>12, 'voornaam'=>$gegevens->voornaam,'email'=>$gegevens->email));
                $gebruikersGroep = Sentry::findGroupByName('gebruikers');
                $gebruiker->addGroup($gebruikersGroep);

                $code = $gebruiker->getActivationCode();
                $naam = $gebruiker->getNaam();
                $email = $gebruiker->email;

                Mail::send('emails.registratieCode', array('code' => $code), function($message) use($email, $naam)
                {
                    $message->to($email, $naam)->subject('Uw registratie code voor LeenMeij');
                });

            return Redirect::to('succes');
/*            }
            else
            {
                throw new Exception(Lang::get('registratie/errors.verschillendwachtwoord'));
                //throw new Exception('De wachtwoorden zijn niet hetzelfde');
            }
*/
        }
//        catch (PDOException $e)
//        {
//            $bericht = $e->getMessage();
//            if (starts_with($bericht, 'SQLSTATE[23505]'))
//            {
//                //echo "<pre>";
//                //die (var_dump(array('registreer' => Lang::get('registratie/errors.bestaandeemail'))));
//                return Redirect::route('registreer')->withErrors(array('registreer' => Lang::get('registratie/errors.bestaandeemail')));
//            }
//        }
//        catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
//        {
//            return Redirect::route('registreer')->withErrors(array('registreer' => $e->getMessage()));
//        }
//        catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
//        {
//            return Redirect::route('registreer')->withErrors(array('registreer' => Lang::get('registratie/errors.geenwachtwoord')));
//        }
//        catch (Cartalyst\Sentry\Users\UserExistsException $e)
//        {
//            return Redirect::route('registreer')->withErrors(array('registreer' => Lang::get('registratie/errors.bestaandegebruikersnaam')));
//        }
//        catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
//        {
//            return Redirect::route('registreer')->withErrors(array('registreer' => $e->getMessage()));
//        }
        catch(\Exception $e)
        {
            /*echo "<pre>";
            die($e->getMessage());*/
            return Redirect::route('registreer')->withErrors($validator);
        }

    }

    public function getActiveer()
    {

        try
        {

            $code = Input::get('code');
            $gebruiker = Sentry::findUserByActivationCode($code);

            if ($gebruiker->attemptActivation($code))
            {
                return View::make('activeer')->with(array('code' => $code));
            }
//            else
//            {
//                return View::make('activeer')->withErrors(array('activeer' => $e->getMessage()));
//            }
            //return View::make('activeer')->with(array('code' => $code));
        }
        catch (\Exception $e)
        {
            //echo "<pre>";
            //die(var_dump($code));
            return View::make('activeer')->withErrors(array('activeer' => $e->getMessage()));
        }
    }
}