<?php

/* 
 * Controller Die zich bezig houd met de Functies van de beheerder
 */

Class AdminController extends Controller
{
    public function showIndex()
    {
        return View::make('AdminView');
    }
    /*
     * Maakt view aan met alle reserveringen en beheerfunctionaliteit
     * Auteur : Martijn
     */
    public function showReserveringen()
    {
        $dbReserveringen = Reservering::all();
        foreach($dbReserveringen as $reservering)
        {
        $reserveringen[$reservering->id]= array('id'=> $reservering->id,'klantenid'=>$reservering->klantenid,'voertuigenid'=>$reservering->voertuigenid,'reserveerdatum'=> $reservering->reserveerdatum,'begindatum'=> $reservering->begindatum,'einddatum'=> $reservering->einddatum,'kilometer'=> $reservering->kilometer,'bedrag'=> $reservering->bedrag,'status'=> $reservering->status);
        }
        return View::make('Admin/AdminReserveringenView')->with('reserveringen',$reserveringen);
    }
    /*
     * Maakt view aan met alle Users en beheerfunctionaliteit
     * Auteur : Martijn
     */
    public function showUsers()
    {
            $users =User::all();
            foreach($users as $user)
            {
                $activatable = ! $user->hasActivationCode();
                $userControl[$user->id] = array(
                    'id'=>$user->id,
                    'voornaam'=>$user->voornaam,
                    'tussenvoegsel'=>$user->tussenvoegsel,
                    'achternaam'=>$user->achternaam,
                    'activated'=>$user->activated,
                    'activatable'=> $activatable
                );
            }
            return View::make('Admin/AdminUsersView')->with('users',$userControl);
                    
    }

    public function deleteUser()
    {
        $userId = Input::get('user');
        $user = Sentry::findUserById($userId);
        $user->delete();

        return Redirect::to('gebruikersoverzicht');
    }

    public function toggleActiveUser()
    {
        $userId = Input::get('user');
        $user = Sentry::findUserById($userId);
        $active = $user->getActivated();
        $user->setActive(! $active);

        return Redirect::to('gebruikersoverzicht');
    }

    /*
     * Maakt view aan met alle Reviews en beheerfunctionaliteit
     * Auteur : Martijn
     */
    public function showReviews()
    {
        $dbreviews = Review::all();
        $reviews= array();
         
        foreach ($dbreviews as $review)
        {

          $user=User::find($review->userid);
            $reviews[$review->id] = array(
                'id'=>$review->id,
                'userid'=>$review->userid,
                'uservoornaam'=>$user->voornaam,
                'usertussenvoegsel'=>$user->tussenvoegsel,
                'userachternaam'=>$user->achternaam,
                'datum'=>$review->created_at,
                'text'=>$review->text,
                'titel'=>$review->titel,
                'voorpagina'=>$review->voorpagina);
        }
         return View::make('Admin/AdminReviewView')->with('reviews',$reviews);
    }
    /*
     * Maakt view aan met alle Inhoud en beheerfunctionaliteit
     * Auteur : Martijn
     */
    public function showInhoud()
    {
      $dbinhouden = Inhoud::all();
      
        foreach($dbinhouden as $kaas)
      {
        $dbinhoud[$kaas->id]=array('id'=>$kaas->id,'beschrijving'=>$kaas->beschrijving,'textnl'=>$kaas->textnl,'texten'=>$kaas->texten);
      }
      return View::make('Admin/AdminInhoudView')->with('inhouden',$dbinhoud);
      
    }
    /*
     * Maakt view aan met alle Thema's en beheerfunctionaliteit
     * Auteur : Martijn
     */
    public function showThemes()
    {
        $dbthemes = Skin::all();
        $thema =array();
        foreach($dbthemes as $themes)
        {
            $thema[$themes->id]=array ('id'=>$themes->id,'naam'=>$themes->naam,'huidigeskin'=>$themes->huidigeskin);
        }
        return View::make('Admin/AdminThemeView')->with('themes',$thema);
    }
     /*
     * zet de status van een reservering als opgehaald
     * Auteur : Martijn
     */
    public function reserveringOpgehaald()
    {
        $id=$_POST['reservering'];
        $reservering = Reservering::find($id);
        $reservering->status = 'opgehaald';
        $reservering->save();
        return Redirect::to('reserveringsoverzicht');
         
    }
    /*
     * zet de status van een reservering als teruggebracht
     * Auteur : Martijn
     */
    public function reserveringTerugGebracht()
    {
     $id=$_POST['reservering'];
        $reservering = Reservering::find($id);
        $reservering->status = 'beschikbaar';
        $reservering->save();
        return Redirect::to('reserveringsoverzicht');
    }
    /*
     * Slaat wijzigingen in Inhoud op
     * Auteur : Martijn
     */
    public function inhoudOpslaan()
    {
        $id=$_POST['inhoud'];
        $inhoud = Inhoud::find($id);
        $inhoud->textnl = $_POST['textnl'];
        $inhoud->texten = $_POST['texten'];
        $inhoud->save();
       
        
        return Redirect::to('inhoudoverzicht')->with('message','Text gewijzigt');
    }
    /*
     * Beveiligd input en slaat Review op
     * Auteur : Martijn
     */
    public function reviewOpslaan()
    {
        $text = strip_tags($_POST['review']);
        $title =strip_tags($_POST['titel']);
        
        $reservering = Reservering::where('klantenid','=',Sentry::getUser()->id)
                                    ->orderBy('id', 'desc')
                                    ->first();
        if(!count($reservering)==0)
        {
            $review = new Review;
            $review->text = $text;
            $review->title = $title;
            $review->userid= Sentry::getUser()->id;
            $review->reservering= $reservering->id;
            $review->save();
        return Redirect::to('/')->with('message','review geplaatst');
        }
        else
        {
           return Redirect::to('/')->with('message','review niet geplaats vanwege gebrek aan ervaring met LeenMeij');
        }
    }
    /*
     * verwijdert Review
     * Auteur : Martijn
     */
    public function reviewVerwijderen()
    {
        $review = Review::find($_POST['review']);
        $review->delete();
                return Redirect::to('reviewsoverzicht');
    }
    /*
     * Plaatst review op hoofdpagina
     * Auteur : Martijn
     */
    public function reviewHoofdpagina()
    {
        $review = Review::find(Input::get('review'));
        $review->voorpagina = 'true';
        $review->save();
        return Redirect::to('reviewsoverzicht');
        
    }
    /*
     * haalt review van hoofdpagina
     * Auteur : Martijn
     */
    public function reviewVanHoofdpaginaAf()
    {
        
      $review = Review::find($_POST['review']);
        $review->voorpagina = 'false';
        $review->save();
                return Redirect::to('reviewsoverzicht');
        
    }
    /*
     * Stelt de gebruikte stylesheet in voor de gebruiker pagina's
     * Auteur : Martijn
     */
    public function cssGebruiken()
    {  
        
        $huidigethema = Skin::all();
        foreach ($huidigethema as $theme)
        {
            if ($theme->huidigeskin =='ja')
            {
                $theme->huidigeskin ='nee';
                $theme->save();
            }
        }
        $thema = Skin::find($_POST['thema']);
        $thema->huidigeskin = 'ja';
        $thema->save();
        return Redirect::to('themesoverzicht');
    }
    /*
     * Slaat een Nieuwe stylesheet op
     * Auteur : Martijn
     */
    public function cssOpslaan()
    {
        $file = Input::file('css');
        $destinationPath = public_path().'/css/';
        $fileName= $_POST['cssnaam'].'.css';
        $file->move($destinationPath, $fileName);
        $skin = new Skin;
        $skin->naam =$_POST['cssnaam'];
        $skin->huidigeskin ='nee';
        $skin->save();
        return Redirect::to('themesoverzicht');
    }
    /*
     * verwijdert een stylesheet
     * Auteur : Martijn
     */
    public function cssVerwijderen()
    {
        $theme = Skin::find($_POST['theme']);
        File::delete(public_path().'/css/'.$theme->naam.'.css');
        $theme->delete();
        return Redirect::to('themesoverzicht');
    }
}