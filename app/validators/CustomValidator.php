<?php
/**
 * Created by PhpStorm.
 * User: Daan
 * Date: 5-2-14
 * Time: 10:45
 */


class CustomValidator
{

    public function phone($field, $value, $params)
    {
        return preg_match('/^[\pN\s+-]+$/u', $value);
    }

    public function name($field, $value, $params)
    {
        return preg_match('/^[\pL\pN\s._-]+$/u', $value);
    }

    public function password($field, $value, $params)
    {
        if( !preg_match("#[0-9]+#", $value) ) {
            return false;
        }

        if( !preg_match("#[a-z]+#", $value) ) {
            return false;
        }

        if( !preg_match("#[A-Z]+#", $value) ) {
            return false;
        }

        return true;
    }

    public function postcode($field, $value, $params)
    {
        if (is_numeric(substr($value, 0, 4)) && ctype_alpha(substr($value, -2)))
        {
            return true;
        }
        return false;
    }

    public function captcha($fields, $value, $params)
    {
        $securimage = new Securimage();
        if ($securimage->check($value) == false) {
            return false;
        }

        return true;
    }
}