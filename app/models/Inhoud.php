<?php
/**
 * Created by PhpStorm.
 * User: Daan
 * Date: 22-1-14
 * Time: 16:29
 */
class Inhoud extends Eloquent
{
    protected $table = 'inhoud';

    public function getText()
    {
        $taal = Config::get('app.locale');
        if (strcmp($taal, 'nl') === 0)
        {
            return $this->textnl;
        }
        else if (strcmp($taal, 'en') === 0)
        {
            return $this->texten;
        }
    }
}