<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
/*
class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	

use Cartalyst\Sentry\Users\Eloquent\User as SentryUserModel;

class User extends SentryUserModel implements UserInterface, RemindableInterface {
protected $table = 'users';
protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}


    public function getNaam()
    {
        if ($this->tussenvoegsel != NULL)
        {
            return $this->voornaam . ' ' . $this->tussenvoegsel . ' ' . $this->achternaam;
        }
        else
        {
            return $this->voornaam . ' ' . $this->achternaam;
        }
    }

    public function getActivated()
    {
        return $this->activated;
    }

    public function setActive($active)
    {
        $this->activated = $active;
        $this->save();
    }

    public function hasActivationCode()
    {
        if ($this->activation_code == null)
            return false;
        else
            return true;
    }


}