<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//Validator::extend('password', 'CustomValidator@password');


/******************************
 * Init Methode naar de Website
 ******************************/
Route::get('/','HomeController@showWelcome');


//Route naar pagina waar inhoud over Leenmeij wordt getoont
Route::get('infopagina','HomeController@showInfo');

//Route naar pagina waar Reviews worden getoont
Route::get('reviewpagina','HomeController@showReviews');

//route's voor het switchen van taal
Route::any('switchNederlands','TaalController@switchNederlands');
Route::any('switchEngels','TaalController@switchEngels');






/*
 * LogIn routes for the custoomer 
 */

/* Use for Signing In */
Route::get('signin', 'LogInController@ShowLoginpagina');

/* Use for customer registration */
Route::get('register', 'LogInController@ShowRegisterFormulier');

/* customer forgets Password */
Route::get('reset', 'LogInController@ShowWachtwoordReset');

/* ========================= */
/* =Reservation by the Cust= */
/* ========================= */

/* Reserveren pagina */
Route::get('reserveer', 'reserveringVoertuigController@show_reserveringspagina');
//route voor het filteren van voertuigen op basis van klant informatie
Route::post('reserveringVoertuig','reserveringVoertuigController@showBeschikbareVoertuigen');

/* ========================= */
/* =Beheren by the Admin= */
/* ========================= */
Route::group(array('before' => 'adminAuth'), function()
{

    /* Use for Themas overview */
    Route::get('themesoverzicht', 'AdminController@showThemes');
    Route::post('cssopslaan','AdminController@cssOpslaan');
    Route::post('cssgebruiken','AdminController@cssGebruiken');
    route::post('cssverwijderen','AdminController@cssVerwijderen');

    /* use for managing the reservtion overview */
    Route::get('reserveringsoverzicht', 'AdminController@showReserveringen');
    Route::post('reserveringOpgehaald','AdminController@reserveringOpgehaald');
    Route::post('reserveringTerugGebracht','AdminController@reserveringTerugGebracht');

    Route::post('userDelete', 'AdminController@deleteUser');
    Route::post('userToggleActivated', 'AdminController@toggleActiveUser');
    /* use for managing the user overview*/
    Route::get('gebruikersoverzicht', 'AdminController@showUsers');

    /* use for managing the review overview*/
    Route::get('reviewsoverzicht', 'AdminController@showReviews');
    Route::post('reviewHoofdpagina','AdminController@reviewHoofdpagina');
    Route::post('reviewVanHoofdpaginaAf','AdminController@reviewVanHoofdpaginaAf');
    Route::post('reviewVerwijderen','AdminController@reviewVerwijderen');

    /* use for managing the Inhoud overview*/
    Route::get('inhoudoverzicht','AdminController@showInhoud');
    Route::post('inhoudWijzigen','AdminController@inhoudOpslaan');

});

Route::get('logintest', function()
{
    return View::make('registratiepagina');
});

Route::get('logout', array('as' => 'logout', 'uses' => 'AuthController@getLogout'));
Route::get('login', array( 'as' => 'login', 'uses' => 'AuthController@getLogin'));
Route::post('login', array('before' => 'csrf', 'as' => 'login.post', 'uses' => 'AuthController@postLogin'));

Route::group(array('before' => 'auth'), function()
{
    // Link naar pagina om review aan te maken
    Route::get('review','HomeController@showReviewMaak');
    //Route voor het opsturen van een Review
    Route::post('reviewOpsturen','AdminController@reviewOpslaan');
    //Route voor het maken van een reservering voor een voertuig
    Route::any('reserveringVoertuigUitvoeren','reserveringVoertuigController@voertuigReserveren');
    
    Route::any('temp', function()
    {
        return View::make('header');
    });
});

Route::get('activeer', 'AuthController@getActiveer');

Route::get('registreer', array('as' => 'registreer', 'uses' => 'AuthController@getRegistreer'));
Route::post('registreer', array('before' => 'csrf', 'as' => 'registreer.post', 'uses' => 'AuthController@postRegistreer'));
Route::get('succes', function()
{
    return View::make('registreerSucces');
});

Route::get('testgebruiker', function()
{
    return View::make('test.gebruiker');
});

Route::get('lang/{lang}', function($lang)
{
    Session::put('my.locale', $lang);
    return Redirect::to('/');
});


Route::get('deleteusers', function()
{
    for ($i = 2; $i < 7; $i++)
    {
        $user = Sentry::findUserById($i);
        $user->delete();
    }
});

/*
Route::get('testmail', function()
{
    $data = "kijken1234";
    Mail::send('emails.registratieCode', array('code' => $data), function($message)
    {
        $message->to('daanschoone@gmail.com', 'Daan Schoone')->subject('Welcome!');
    });
});


 * Dit alleen gebruiken als de database weer leeg is en de admin gebruiker moet weer aangemaakt worden.
 *

Route::get('/seed', function()
{
    $user = Sentry::createUser(array(
        'gebruikersnaam'  => 'admin',
        'email'     => 'admin@admin.com',
        'password'  => 'test',
        'activated' => true,
    ));

    $group = Sentry::createGroup(array(
        'name'        => 'admin',
        'permissions' => array(
            'admin'         => 1,
            'gebruikers'    => 1,
        ),
    ));

    $user->addGroup($group);

    $groep = Sentry::createGroup(array(
        'name'          => 'gebruikers',
        'permissions'   =>  array(
            'admin'         => 0,
            'gebruikers'    => 0,
        ),
    ));

});

*/

