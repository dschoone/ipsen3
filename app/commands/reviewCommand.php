<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class reviewCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:reviewCommand';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$reserveringen = Reservering::all();
$reviewDate = date('Y-m-d', strtotime('+5 days'));
foreach ($reserveringen as $reservering)
{
    $user = User::find($reservering->klantenid);
    $voertuig = Voertuig::find($reservering->voertuigenid);
    if ($reservering->eindDatum ==  $reviewDate)
    {
        $data = array('reserveerDatum'=> $reservering->reserveerDatum,'beginDatum'=>$reservering->begindatum,'eindDatum'=>$reservering->einddatum,'voertuig'=>Voertuig::find($voertuigID)->beschrijving);
        Mail::send('emails.review',$data,function($message) use($reservering)
        {
             $message->to($user->email, $user->voornaam." ".$user->tussenvoegsel." ".$user->achternaam)->subject('Hoe is uw voertuig bevallen?');
        });
    }
}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	/*protected function getArguments()
	{
		return array(
			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	/*protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}*/

}
