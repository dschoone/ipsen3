<?php
/**
 * Created by PhpStorm.
 * User: Daan
 * Date: 22-1-14
 * Time: 14:35
 */
?>
<div id="loginknop">
    @if (Sentry::check())
        <button type="button">{{Lang::get('knoppen.loguit')}}</button>
    @else
        <button type="button">{{Lang::get('knoppen.login')}}</button>
    @endif
</div>