@extends('Master')


@section('content')
<?php
if (isset($message)==true)
{
    echo '<div id="error"><p>'.$message.'</p></div>';
}
 
 ?>
    <!-- The Advertisment --> 
        
      <div id="advertisment">
           <div class="innerAdvert left">
               <div class="title">
                   
                    <h1> {{Lang::get('welcomepage.aanbod')}}</h1>
                      

                    <p>  <?php 
                    if (Config::get('app.locale')== 'nl')
                    {echo $inhoud->textnl;}
                    if (Config::get('app.locale')== 'en')
                    {echo $inhoud->texten;}?> </p>
                    <p></p>
                </div> 
               <div class="btns"><a href="infopagina"> {{Lang::get('welcomepage.meerinfo')}}</a></div>
          </div>          
          
           <div class="innerAdvert">
              <div class="title">
                    <h1> Reviews</h1>
                </div> 
               <?php
               foreach ($reviews as $review)
               {
                   echo $review['titel']."<br>";
                   echo $review['tekst'] ."<br>";
                   
               } ?>
                 <p> {{Lang::get('welcomepage.geenrev')}}</p>
           <div class="btns"><a href="reviewpagina"> {{Lang::get('welcomepage.meerinfo')}}</a></div>

          </div>
          <div class="innerAdvertContact">
              <div class="title">
                <h1> Contact</h1>
              </div> 
              <div class="icon">   		  

                <p><a href="#"> <img src="image/email_ico.png" alt="" /><strong>Email</strong> info@leenmeij.nl</a></p>
                <p><a href="#"> <img src="image/fscebook_ico.png" alt="" /><strong>Facebook</strong>{{Lang::get('welcome.volgons')}}Facebook </a></p>
                <p><a href="#"><img src="image/twitter_ico.png" alt="" /><strong>Twitter</strong>{{Lang::get('welcome.volgons')}}Twitter </a></p>
               		         
		<br><br>			
              </div>
          </div>
          
      </div>
       
@stop

