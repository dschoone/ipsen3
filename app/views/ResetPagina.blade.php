@extends('Master') 
<?php
         echo HTML::style('css/login.css');
                 
        ?>

@section('content')
    <div class="LogIncontainer">
			<section id="content">
                {{Form::open(array('route' => 'login.post'))}}
				<!--<form action="">-->
					<h1>Login Form</h1>
					<div>
                        {{Form::text('gebruikersnaam', null, array('placeholder' => Lang::get('reset.email'), 'id' => 'username'))}}
						<!--<input type="text" placeholder="Username" required="" id="username" />-->
					</div>					
					<div>
                        {{Form::submit(Lang::get('knoppen.verzend'))}}
						
					</div>
                    {{Form::close()}}
				<!--</form> form -->
			</section><!-- content -->
	</div><!-- container -->

        <!-- Als inlog mis gaat --> 

        @if ($errors->has('login'))
        <div id="error">{{ $errors->first('login', ':message') }}</div>
        @endif
        
@stop