@extends('Master') 

@section('content')

<?php

echo "<ul id='picturelist'>";

foreach ($beschikbareVoertuigen as $voertuig)
{    
    echo "<li>" . $voertuig['merk'] ." " . $voertuig['beschrijving'] . " ". $voertuig['kleur']. 
       HTML::image('image/'.$voertuig['beschrijving'].'.jpg','Voertuig Plaatje' ,
                    array('width'=>'150','height' =>'auto'))
              . 'kost'  ." : ". $voertuig['bedrag'] .                       
     "</li>";

 
  if ($voertuig['reserveren']== true)
  {
      echo Form::open(array('url' => 'reserveringVoertuigUitvoeren'));
  echo Form::hidden('voertuig',$voertuig['id']);
  
  echo "<td>" .Form::submit('Reserveer mij')."</td>";
  echo Form::close();
  }
}
 
?>
@stop
