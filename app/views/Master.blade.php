<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html lang="en">
<head>
    <title>LeenMeij</title>  
    <?php
        echo HTML::style('css/style.css'); 
        $css = Skin::all();
        foreach($css as $theme)
        {
             if ($theme->huidigeskin == 'ja' )
             {
                echo HTML::style('css/'.$theme->naam.'.css');
             }
         
         }
    
     //echo HTML::style('css/stylesheet_1.css');
    ?>
    
    <script type="text/javascript" src="scripts/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="scripts/jquery.nivo.slider.js"></script>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
    <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider();
    });
    </script>
</head>
<body>
  
<div id="container">    
<!-- Header --> 
    <div id="wrapper">
        <div class="slider-wrapper theme-default"> 
            <!-- Logo --> 
            <div id="logo" onclick="location.href='{{ URL::to('/') }}'" >
                <img src="image/Logo-Klein.png" alt="" />
            </div>

            @if (Sentry::check())
                <?php
                $gebruiker = Sentry::getUser();
                ?>
                <button id="menu-btn" onclick="location.href='{{ URL::to('logout') }}'" type="button">{{Lang::get('knoppen.loguit', array('naam' => $gebruiker->voornaam))}}<img src="image/account.png" alt="" /></button>
            @else
                <button id="menu-btn" onclick="location.href='{{ URL::to('login') }}'" type="button">{{Lang::get('knoppen.login')}}<img src="image/account.png" alt="" /></button>
            @endif

            <button id="menu-btn" onclick=" location.href='{{URL::to('switchNederlands')}}' " type="button">Nederlands<img src="image/nlLogo.png" alt="" /></button>
            <button id="menu-btn" onclick=" location.href='{{URL::to('switchEngels')}}' " type="button">English<img src="image/ukLogo.png" alt="" /></button>

            <button id="btnReserveer" onclick="location.href='{{ URL::to('reserveer') }}'" type="button">{{Lang::get('knoppen.reserveer')}}</button>           
            <div id="slider" class="nivoSlider">                  
                <img src="image/Banner1.jpg" data-thumb="image/Banner1.jpg" alt="" />
				<img src="image/Banner3.jpg" data-thumb="image/Banner3.jpg" alt="" title="This is an example of a caption" />
                <img src="image/Banner4.jpg" data-thumb="image/Banner4.jpg" alt="" data-transition="slideInLeft" />
                <img src="image/Banner5.jpg" data-thumb="image/Banner5.jpg" alt="" title="#htmlcaption" />                
            </div>                 
        </div>        
    </div>  
<!-- Content -->   

        @yield('content')       
<!-- Footer -->
    <div id="footer">
        <hr size=4 width="80%">
        <div class="copyright">&copy; 2013 Copyright LEENMEIJ. All Rights Reserved[<a href="#" STYLE="color:#000000;" >LeenMeij</a>].</div>
    </div>
</div>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
</body>
</html>