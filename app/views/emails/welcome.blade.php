<?php
/**
 * Created by PhpStorm.
 * User: Daan
 * Date: 15-1-14
 * Time: 16:13
 */
?>
@extends('template')
<?php
         echo HTML::style('css/login.css');
//echo $data;
        ?>

@section('content')
<div class="container">
    <section id="content">
        {{Form::open(array('route' => 'login.post'))}}
        <!--<form action="">-->
        <h1>Login Form</h1>
        <div>
            {{Form::text('gebruikersnaam', null, array('placeholder' => 'Gebruikersnaam', 'id' => 'username'))}}
            <!--<input type="text" placeholder="Username" required="" id="username" />-->
        </div>
        <div>
            {{Form::password('password', array('placeholder' => 'Wachtwoord', 'id' => 'password'))}}
            <!--<input type="password" placeholder="Password" required="" id="password" />-->
        </div>
        <div>
            {{Form::submit('Log in')}}
            <!--<input type="submit" value="Log in" />-->
            <a href="#">Lost your password?</a>
            <a href="#">Register</a>
        </div>
        {{Form::close()}}
        <!--</form> form -->
    </section><!-- content -->
</div><!-- container -->

<!-- Als inlog mis gaat -->

<div id="error">
    <!-- hier jouw codes -->
</div>

@stop