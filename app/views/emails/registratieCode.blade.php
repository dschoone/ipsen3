<!DOCTYPE HTML>
<html>
<head>
    <title>LeenMeij Online</title>
    <?php
    // echo HTML::style('css/login.css');
    echo HTML::style('css/stylesheet.css');
    echo HTML::style('css/bjqs.css');
    echo HTML::style('css/demo.css');
    $url = URL::action('AuthController@getActiveer', array('code' => $code));
    ?>
    {{HTML::style('css/login.css')}}
</head>
<body>
<h3>Bedankt voor het registreren bij Leenmeij.</h3>
<p>Om uw registratie te voltooien dient u op de volgende link te klikken. Dit zal u mee terug nemen naar de website van Leenmeij en uw account zal geregistreerd worden.</p>

<a href="{{$url}}">{{$url}}</a>
<p>Indien u zich niet heeft geregistreerd bij Leenmeij, kunt u deze e-mail als niet verzonden beschouwen. Het account waar dit e-mail adres aan gekoppels is zal niet gebruikt kunnen worden zolang u niet op de bovenstaande link klikt.</p>

<p>Hopende u voldoende te hebben geinformeerd.</p>
<p>Met vriendelijke groet,<br>Verhuurbedrijf Leenmeij.</p>

</body>