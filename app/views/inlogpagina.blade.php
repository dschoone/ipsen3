@extends('Master') 
<?php
         echo HTML::style('css/login.css');
                 
        ?>
@section('content')

        <!-- Als inlog mis gaat --> 
        

    <div class="LogIncontainer">
        @if ($errors->has('login'))
            <div id="error"><p>{{$errors->first('login')}}</p></div>
        @endif
        
			<section id="content">
                {{Form::open(array('route' => 'login.post'))}}
				<!--<form action="">-->
					<h1>Login Form</h1>
					<div>
                        {{Form::text('gebruikersnaam', null, array('placeholder' => Lang::get('registratie/formulier.gebruikersnaam'), 'id' => 'username'))}}
						<!--<input type="text" placeholder="Username" required="" id="username" />-->
					</div>
					<div>
                        {{Form::password('wachtwoord', array('placeholder' => Lang::get('registratie/formulier.wachtwoord', array('extra' => '')), 'id' => 'password'))}}
						<!--<input type="password" placeholder="Password" required="" id="password" />-->
					</div>
					<div>
                        {{Form::submit('Log in')}}
						<!--<input type="submit" value="Log in" />-->
						<!--<a href="{{ URL::to('reset') }}">Lost your password?</a>-->
						<a href="{{ URL::to('registreer') }}">Register</a>
					</div>
                    {{Form::close()}}
				<!--</form> form -->
			</section><!-- content -->
	</div><!-- container -->       
        
@stop