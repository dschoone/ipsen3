@extends('Master')

@section('content')
<div id="error" >
{{ Session::get('message') }} 
</div>
<div id="maincontent">
      <div class="title" > <h2> {{Lang::get('reservering/formulier.beschikbaarlijst')}} </h2> </div>
      <div id="filter"> 
           <div class="title" > <h2> Filter </h2> </div>
           <!-- HTML FORM -->
           {{Form::open(array('url' => 'reserveringVoertuig','method' => 'POST'))}}
               <table >
                   <tr>
                       <td> {{Lang::get('reservering/formulier.begindatum')}}:</td>
                       <td> <input type="date" name="beginDatum" required/> </td>
                   </tr>
                   <tr>
                       <td> {{Lang::get('reservering/formulier.einddatum')}}:</td>
                       <td><input type="date" name="eindDatum" required/></td>
                   </tr>
                   <tr>
                       <td> {{Lang::get('reservering/formulier.kilometers')}}:</td>
                       <td><input type ="number" name="kilometers" value="100" required/></td>
                   </tr>
                   <tr>
                       <td> {{Lang::get('reservering/formulier.voertuig')}}:</td>
                       <td>
                           <select name="voertuigType">
                              <option value="Scooter">{{Lang::get('reservering/formulier.scooter')}}</option>
                              <option value="Personen auto" selected>{{Lang::get('reservering/formulier.auto')}}</option>
                              <option value="Bus">{{Lang::get('reservering/formulier.bus')}}</option>
                           </select>
                       </td>                       
                   </tr>
                   <tr>
                       <td> {{Lang::get('reservering/formulier.zakelijkkeuze')}}</td>
                           <td> 
                           <select name="zakelijk">
                               <option value ="zakelijk">{{Lang::get('reservering/formulier.zakelijk')}}</option>
                               <option value ="particulier" selected>{{Lang::get('reservering/formulier.particulier')}}</option>
                           </select>
                           
                       </td>
                   </tr>
               </table>
           <input id ="reservebutton" type = "submit" value="Submit" />
           </form>    
      </div>
      
      <!-- Gallery --> 
      <div id="gallery">
      
        @foreach($beschikbareVoertuigen as $voertuig) 
          <div class="img">
            <div class="desc"> <?php echo $voertuig['merk'] .'<br> '. $voertuig['beschrijving'] .' '. $voertuig['kleur']. ' '. 'Kost' ." : ". $voertuig['bedrag']. ' '; ?> </div>
            <img src="<?php echo 'image/'.$voertuig['beschrijving'].'.jpg' ;?>" 
               alt="<?php echo $voertuig['merk'] .' '. $voertuig['beschrijving'] ;?>" width="150" height="150">          
            <?php
            
            if ($voertuig['reserveren']== true)
             {
                echo Form::open(array('url' => 'reserveringVoertuigUitvoeren'));
                echo Form::hidden('voertuig',$voertuig['id']);
                echo Form::submit('Reserveer mij');              
                echo Form::close();
             }
             ?>           
           </div>
         @endforeach 
                
      </div>
          
        <?php
//           echo "<ul id='picturelist'>";
//
//           foreach ($beschikbareVoertuigen as $voertuig)
//           {
//               echo "<li>" . $voertuig['merk'] ." " . $voertuig['beschrijving'] . " ". $voertuig['kleur']. 
//                  HTML::image('image/'.$voertuig['beschrijving'].'.jpg','Voertuig Plaatje' ,
//                               array('width'=>'150','height' =>'auto'))
//                         . 'kost'  ." : ". $voertuig['bedrag'] .                       
//                "</li>";
//
//             if ($voertuig['reserveren']== true)
//             {
//                 echo Form::open(array('url' => 'reserveringVoertuigUitvoeren'));
//             echo Form::hidden('voertuig',$voertuig['id']);
//
//             echo Form::submit('Reserveer mij');
//              echo "</ul>" ; 
//             echo Form::close();
//             }
//           } 
       ?>
  
</div>
@stop