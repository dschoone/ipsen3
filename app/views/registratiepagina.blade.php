@extends('Master')

@section('content')

<div id="maincontent">
 <!-- Content Register --> 
 
       <div class="title">
          <h1>{{Lang::get('registratie/formulier.registratie')}}</h1>
      </div>

        @if($errors->has('registreer'))
            <div id="error">
                <p>{{$errors->first('registreer')}}</p>
            </div>
        @endif

        <div id="reserveertable">
            
        {{Form::open(array('route' => 'registreer.post'))}}
	<table id="table">
          <tr>
              <td >  </td>
              <td class="wide">
                {{Lang::get('registratie/formulier.gebruikersnaam')}} <br>
                {{Form::text('gebruikersnaam')}}
                <br>
                {{ $errors->first('gebruikersnaam', '<span class="error">:message</span>') }}
              </td>
              <td>
                {{Lang::get('registratie/formulier.wachtwoord', array('extra' => ''))}}<br>
                {{Form::password('wachtwoord')}}							
                <br>
                {{ $errors->first('password', '<span class="error">:message</span>') }}
              </td>
              <td >
                {{Lang::get('registratie/formulier.wachtwoord', array('extra' => Lang::get('registratie/formulier.herhaal')))}}	<br>
                {{Form::password('wachtwoord2')}}                               
                <br>
                {{ $errors->first('password2', '<span class="error">:message</span>') }}
              </td>
           </tr> 
           <tr >
                <td > </td>
                <td >
                  {{Lang::get('registratie/formulier.voornaam')}}<br>
                  {{Form::text('voornaam')}}                                   
                <br>
                {{ $errors->first('voornaam', '<span class="error">:message</span>') }}
                </td>
                <td class="wide">
                  {{Lang::get('registratie/formulier.tussenvoegsel')}}<br>
                  {{Form::text('tussenvoegsel')}}                                  
                <br>
                {{ $errors->first('tussenvoegsel', '<span class="error">:message</span>') }}
                </td>
                <td class="wide">
                  {{Lang::get('registratie/formulier.achternaam')}}<br>
                  {{Form::text('achternaam')}}                                  
                <br>
                {{ $errors->first('achternaam', '<span class="error">:message</span>') }}
                </td>
             </tr>
             <tr>
                <td class=""></td>
                <td class="wide">
                     {{Lang::get('registratie/formulier.zakelijk')}}<br>
                     {{Form::checkbox('zakelijk', 'true')}}
                </td>
                <td class="wide">
                  {{Lang::get('registratie/formulier.kvk')}}<br>
                  {{Form::text('kvk')}}                                    
                <br>
                {{ $errors->first('kvk', '<span class="error">:message</span>') }}
                </td>
                <td class="wide">
                  {{Lang::get('registratie/formulier.bedrijfsnaam')}}<br>
                  {{Form::text('bedrijfsnaam')}}                                    
                <br>
                {{ $errors->first('bedrijfsnaam', '<span class="error">:message</span>') }}
                </td>
             </tr>
             <tr>
                <td class=""> </td>
                <td class="wide">
                  {{Lang::get('registratie/formulier.straatnaam')}}<br>
                  {{Form::text('straatnaam')}}                               
                <br>
                {{ $errors->first('straatnaam', '<span class="error">:message</span>') }}
                </td>
                <td class="wide">
                  {{Lang::get('registratie/formulier.huisnummer')}}<br>
                  {{Form::text('huisnummer')}}                               
                <br>
                {{ $errors->first('huisnummer', '<span class="error">:message</span>') }}
                </td>
                <td class="wide">
                  {{Lang::get('registratie/formulier.postcode')}}<br>
                  {{Form::text('postcode')}}
                <br>
                {{ $errors->first('postcode', '<span class="error">:message</span>') }}
                  </td>
              </tr>
              <tr>  
               <td class="">  </td>
               <td class="wide">
                 {{Lang::get('registratie/formulier.email')}}<br>
                 {{Form::text('email')}}                    
                <br>
                {{ $errors->first('email', '<span class="error">:message</span>') }}
               </td>
               <td class="wide">
                  {{Lang::get('registratie/formulier.telefoonnummer')}}<br>
                  {{Form::text('telefoonnummer')}}
                <br>
                {{ $errors->first('telefoonnummer', '<span class="error">:message</span>') }}
               </td>
               <td class="wide">
                 {{Lang::get('registratie/formulier.mobielnummer')}}<br>
                 {{Form::text('mobielnummer')}}
                <br>
                {{ $errors->first('mobielnummer', '<span class="error">:message</span>') }}
               </td>
              </tr>
                 <tr>
                            <td class="">    </td>
                            <td class="wide">
                                {{lang::get('registratie/formulier.captcha')}}<br>
                                <div id="captchadiv">
                                    <img id="captcha" src="securimage/securimage_show.php" alt="CAPTCHA Image" />
                                    <div>
                                        {{Form::text('captcha_code', null, array('size' => '10', 'maxlength' => '6'))}}
                                        <!--<input type="text" name="captcha_code" size="10" maxlength="6" />-->
                                        <a href="#" onclick="document.getElementById('captcha').src = 'securimage/securimage_show.php?' + Math.random(); return false">{{Lang::get('registratie/formulier.anderecaptcha')}}</a>
                                    </div>
                                    {{ $errors->first('captcha_code', '<span class="error">:message</span>') }}
                                </div>
                            </td>
                            <td class="wide">
                                {{Form::submit(Lang::get('registratie/formulier.verstuur'))}}
                            </td>
                        </tr>
                            <tr>
                                {{Lang::get('registratie/formulier.persoonlijkeinformatie')}}
                            </tr>

                        </table>               
            {{Form::close()}}   
            </div>
            <div id="twitterfeed">
                <div class="title" > <h2> {{lang::get('registratie/formulier.tweet')}} </h2></div>
            </div> 

</div>

    @stop
