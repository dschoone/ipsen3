@extends('Admin/Admin')

@section('content')  

<div id="overzichtcontent">
    <?php
    echo "<table border='1'>
    <tr>
    <th>Id</th>
    <th>Userid</th>
    <th>Voornaam</th>
    <th>Tussenvoegsel</th>
    <th>Achternaam</th>
    <th>Plaatsingsdatum</th>
    <th>Text</th>
    <th>Voorpagina</th>
    <th>Plaats op voorpagina</th>
    <th>Haal van voorpagina</th>
    <th>Verwijder</th>
    </tr>";
    foreach($reviews as $review)
    {
        echo "<tr>";
      echo "<td>" . $review['id'] . "</td>";
      echo "<td>" . $review['userid'] . "</td>";
      echo "<td>" . $review['uservoornaam'] . "</td>";
      echo "<td>" . $review['usertussenvoegsel'] . "</td>";
      echo "<td>" . $review['userachternaam'] . "</td>";
       echo "<td>" . $review['datum'] . "</td>";
       echo "<td>" . $review['text']. "</td>";

        if ($review['voorpagina'])
        {
            echo "<td>Ja</td>";
        }
        else
        {
            echo "<td>Nee</td>";
        }

      echo Form::open(array('url' => 'reviewHoofdpagina'));
        echo Form::hidden('review',$review['id']);
        echo "<td>" .Form::submit('zet review op hoofdpagina')."</td>";
      echo Form::close();
      echo Form::open(array('url' => 'reviewVanHoofdpaginaAf'));
        echo Form::hidden('review',$review['id']);
        echo "<td>" .Form::submit('Haal review van hoofdpagina')."</td>";
      echo Form::close();
      echo Form::open(array('url' => 'reviewVerwijderen'));
        echo Form::hidden('review',$review['id']);
        echo "<td>" .Form::submit('verwijder Review')."</td>";
      echo Form::close();
      echo "</tr>";
    }
    echo"</table>";

    ?>
</div>

@stop