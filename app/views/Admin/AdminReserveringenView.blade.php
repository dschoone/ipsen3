@extends('Admin/Admin')

@section('content')  

<div id="overzichtcontent">    
    <?php
    echo "<table border='1'>
    <tr>
    <th>Id</th>
    <th>Klantenid</th>
    <th>Voertuigenid</th>
    <th>Reserveerdatum</th>
    <th>Begindatum</th>
    <th>Einddatum</th>
    <th>Kilometers</th>
    <th>Bedrag</th>
    <th>Status</th>
    <th>Opgehaald</th>
    <th>Terug gebracht</th>
    </tr>";


foreach ($reserveringen as $reservering)
{
  echo "<tr>";
  echo "<td>" . $reservering['id'] . "</td>";
  echo "<td>" . $reservering['klantenid'] . "</td>";
  echo "<td>" . $reservering['voertuigenid'] . "</td>";
  echo "<td>" . $reservering['reserveerdatum'] . "</td>";
  echo "<td>" . $reservering['begindatum'] . "</td>";
  echo "<td>" . $reservering['einddatum'] . "</td>";
  echo "<td>" . $reservering['kilometer'] . "</td>";
  echo "<td>" . $reservering['bedrag'] . "</td>";
  echo "<td>" . $reservering['status'] . "</td>";
  
  echo Form::open(array('url' => 'reserveringOpgehaald'));
    echo Form::hidden('reservering',$reservering['id']);
    echo "<td>" .Form::submit('Opgehaald')."</td>";
  echo Form::close();
  echo Form::open(array('url'=>'reserveringTerugGebracht'));
  echo Form::hidden('reservering',$reservering['id']);
  echo "<td>".Form::submit('Terug Gebracht')."</td>";
  echo Form::close();
  echo "</tr>";
}
echo"</table>";

?>


@stop