@extends('Admin/Admin')

@section('content')  

<div id="overzichtcontent">
    <?php
    echo "<table border='1'>
    <tr>
    <th>Id</th>
    <th>naam</th>
    <th>in Gebruik</th>
    <th>Gebruik</th>
    <th>Verwijder</th>
    
    </tr>";
    foreach($themes as $thema)
    {
        echo "<tr>";
      echo "<td>" . $thema['id'] . "</td>";
      echo "<td>" . $thema['naam'] . "</td>";
      echo "<td>" . $thema['huidigeskin'] . "</td>";

      echo Form::open(array('url' => 'cssgebruiken'));
        echo Form::hidden('thema',$thema['id']);
        echo "<td>" .Form::submit('gebruik deze stylesheet')."</td>";
      echo Form::close();
      echo Form::open(array('url'=>'cssverwijderen'));
      echo Form::hidden('theme',$thema['id']);
      echo"<td>" .Form::submit('verwijder deze stylesheet')."</td>";
      echo Form::close();
     
      echo "</tr>";
    }
    echo"</table>";
    echo Form::open(['url'=>'cssopslaan', 'files'=>true]);
    echo Form::file('css');
    echo Form::text('cssnaam','geef naam nieuwe stylesheet');
    echo Form::submit('Upload Nieuwe Stylesheet');
    echo Form::close();
    ?>
</div>

@stop