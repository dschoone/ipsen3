<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html lang="en">
<head>
    <title>LeenMeij AdminPage</title>  
    <?php
     echo HTML::style('css/style.css'); 
     echo HTML::style('css/stylesheet.css');
    ?>    
</head> 
<body>
 <div class="nameSpan effect">
	<h1>LeenMeij.NL</h1>
</div>
<div id="container">     
    <!-- Content --> 
    <div id="contentadmin"> 
         <div id="titleadmin"> <h2> LeenMeij AdminPage</h2></div>
        <!-- Navigation Bar --> 
             <div id="navigation">
                 <div id="btnholder">
                     <div class="navbtns" onclick="location.href='{{ URL::to('') }}'"> Hoofdpagina </div>
                     <div class="navbtns" onclick="location.href='{{ URL::to('themesoverzicht') }}'"> Thema`s </div>
                     <div class="navbtns" onclick="location.href='{{ URL::to('reserveringsoverzicht') }}'"> Reserverings Overzicht </div>
                     <div class="navbtns" onclick="location.href='{{ URL::to('inhoudoverzicht') }}'"> Inhoud Overzicht </div>
                     <div class="navbtns" onclick="location.href='{{ URL::to('gebruikersoverzicht') }}'"> Gebruikers Overzicht </div>
                     <div class="navbtns" onclick="location.href='{{ URL::to('reviewsoverzicht') }}'"> Reviews Overzicht </div>
                     <div class="navbtns" onclick="location.href='{{ URL::to('logout') }}'"> Uitloggen </div>
                 </div>                 
             </div>
        
        @yield('content')
        
    </div>
    <!-- Footer -->
    <div id="footer">
        <hr size=4 width="80%">
        <div class="copyright">&copy; 2013 Copyright LEENMEIJ. All Rights Reserved[<a href="#" STYLE="color:#000000;" >LeenMeij</a>].</div>
    </div>
</div>
   
</body>
</html>