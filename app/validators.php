<?php
/**
 * Created by PhpStorm.
 * User: Daan
 * Date: 5-2-14
 * Time: 10:46
 */
Validator::extend('password', 'CustomValidator@password');
Validator::extend('captcha', 'CustomValidator@captcha');
Validator::extend('postcode', 'CustomValidator@postcode');
Validator::extend('name', 'CustomValidator@name');
Validator::extend('phone', 'CustomValidator@phone');